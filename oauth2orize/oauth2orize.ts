import oauth2orize from "oauth2orize-koa";
import {v4 as uuidv4} from "uuid";
import {AuthorizationCodeService} from "../mock-db/mock-authorization-code.service";
import {AccessTokenService} from "../mock-db/mock-access-token.service";
import {ClientService} from "../mock-db/mock-client.service";
import { createHash } from "crypto";
import Client from "../models/client.model";
import pkce from "oauth2orize-pkce"

const server = oauth2orize.createServer({userProperty: 'user'});

server.grant(pkce.extensions());

server.grant(oauth2orize.grant.code(async (client: Client, redirectUri, user, ares, req) => {
    const {codeChallenge, codeChallengeMethod} = req;
    const code = uuidv4();
    AuthorizationCodeService.create(
        code,
        client.clientId,
        redirectUri,
        user.id,
        ares.scope,
        codeChallenge,
        codeChallengeMethod
    );
    return code;
}));

server.exchange(oauth2orize.exchange.code((client: Client, code: string, redirectURI: string, req: any) => {
    const {code_verifier: codeVerifier} = req;
    const authorizationCode = AuthorizationCodeService.getBy('code', code);
    if (!authorizationCode) throw new Error('Invalid code');
    if (authorizationCode.clientId !== client.clientId) throw new Error('Client ID mismatch!');
    if (authorizationCode.redirectURI !== redirectURI) throw new Error('Redirect URI mismatch!');
    if (authorizationCode.codeChallenge) {
        if (!codeVerifier) {
            throw new Error('Missing code verifier!');
        }
        const hashFunction = createHash('sha256');
        const hex = hashFunction.update(codeVerifier).digest('hex');
        const codeChallenge = Buffer.from(hex).toString('base64');
        if (authorizationCode.codeChallenge !== codeChallenge) {
            throw new Error('Code challenge failed!');
        }
    }
    const token = uuidv4() + uuidv4();
    AccessTokenService.create(token, authorizationCode.userId, authorizationCode.clientId, authorizationCode.scope);
    return token;
}));

server.serializeClient((client) => {
    return client.id;
});

server.deserializeClient((id) => {
    const client = ClientService.getBy('id', id);
    if (!client) {
        throw new Error('Invalid client id!');
    }
    return client;
})

export const authorizationServer = server;
