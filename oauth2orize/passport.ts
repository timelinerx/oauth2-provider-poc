import passport from "koa-passport";
import {BasicStrategy} from "passport-http";
import {Strategy as ClientPasswordStrategy} from "passport-oauth2-client-password";
import {Strategy as BearerStrategy} from "passport-http-bearer";
import {ClientService} from "../mock-db/mock-client.service";
import {AccessTokenService} from "../mock-db/mock-access-token.service";
import {UserService} from "../mock-db/mock-user.service";
import {Strategy as PublicClientStrategy} from 'passport-oauth2-client-public'

function authenticateClient(clientId: string, clientSecret: string, done: Function) {
    const client = ClientService.getBy('clientId', clientId);
    if (!client || client.clientSecret !== clientSecret) {
        return done(null, false);
    }
    return done(null, client);
}

passport.use(new BasicStrategy(authenticateClient));

passport.use(new ClientPasswordStrategy(authenticateClient));

passport.use(new PublicClientStrategy((clientId, done) => {
    const client = ClientService.getBy('clientId', clientId);
    if (!client) {
        return done(null, false);
    }
    return done(null, client);
}));

passport.use(new BearerStrategy((token, done) => {
    const accessToken = AccessTokenService.getBy('token', token);
    if (!accessToken) {
        return done(null, false);
    }
    const user = UserService.getBy('id', accessToken.userId);
    if (!user) {
        return done(null, false);
    }
    return done(null, user, { scope: 'all'});
}));
