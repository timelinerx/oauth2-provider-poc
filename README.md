# OAuth2 Provider - Proof of Concept

PoC Implementation of the oauth2orize-koa package.

## Installation
1. Clone the repository
2. `npm i`

## Usage
- `npm run build`
- `npm start`

***

* Open http://localhost:1337/authorize?response_type=code&client_id=test-client-id&redirect_uri=http://127.0.0.1:8080/

* When prompted, log-in with the hard-coded user credentials:
      * *(you can add users to ./mock-db/filler.ts or register at http://localhost:1337/register)*

```text
email: testuser@asd.com
password: p4ssw0rd
```

* Click approve, and you will be redirected to the `redirect_uri` with the `authorization-code` as a query param.

* Send a `POST` request to `/token`, with a Basic Authorization header, where the username is the `clientId`
  and the password is the `clientSecret` (`./mock-db/filler.ts - clients`).

      * *replace `code` with the actual `authorization-code`*

```
POST /token HTTP/1.1
Host: http://localhost:1337
Authorization: Basic <<BASE64(CLIENT_ID:CLIENT_SECRET)>>
Content-Type: application/json
 
{
   "grant_type": "authorization_code",
   "code" : "<<AUTHORIZATION_CODE>>",
   "client_id": "test-client-id",
   "redirect_uri": "http://127.0.0.1:8080/"
}
```

* Use the received `access_token` to access protected endpoints for example: `GET /api/userinfo` 
  via sending the `access_token` as Bearer Token Authorization header with the request.
```
GET /api/userinfo HTTP/1.1
Host: http://localhost:1337
Authorization: Bearer <<ACCESS_TOKEN>>
```
