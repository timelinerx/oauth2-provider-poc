import Koa from 'koa';
import Morgan from 'koa-morgan';
import bodyParser from "koa-bodyparser";
import session from "koa-session";
import router from "./api/routes";
import "./mock-db/filler";
import views from "koa-views";
import './mock-db/filler';
import './oauth2orize/passport';
import cors from "@koa/cors";

const app = new Koa();

const PORT = 1337;

const render = views(__dirname + '/views', {
    extension: 'hbs',
    map: {hbs: 'handlebars'}
});

app.keys = ['titok'];

app.use(session(app));

app.use(render);

app.use(cors());

app.use(Morgan('dev'));

app.use(bodyParser());

app.use(router.routes()).use(router.allowedMethods());

app.listen(PORT, () => {
    console.log(`App is listening port ${PORT}`);
});
