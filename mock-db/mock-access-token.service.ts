import MockService from "./mock.service";
import AccessToken from "../models/access-token.model";
import {accessTokens} from "./mock-db";

class MockAccessTokenService extends MockService<AccessToken> {
    create(...args: ConstructorParameters<typeof AccessToken>): AccessToken {
        const token = new AccessToken(...args);
        accessTokens.push(token);
        return token;
    }

    delete(id: string): void {
        accessTokens.remove(id);
    }

    getBy(key: keyof AccessToken, value: AccessToken[keyof AccessToken]): AccessToken {
        return accessTokens.findBy(key, value);
    }

    update(id: string, updateValues: Partial<AccessToken>): AccessToken {
        let token = accessTokens.findBy('id', id);
        token = {...token, ...updateValues};
        return token;
    }
}

export const AccessTokenService = new MockAccessTokenService();
