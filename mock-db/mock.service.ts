import Model from "../models/model";

export default abstract class MockService<T extends Model> {
    abstract create(...args: any[]): T;
    abstract getBy(key: keyof T, value: T[keyof T]): T;
    abstract update(id: string, updateValues: Partial<T>): T;
    abstract delete(id: string): void;
}
