import {ClientService} from "./mock-client.service";
import {UserService} from "./mock-user.service";

const clients = [
    {
        appName: 'Test Client Application',
        clientId: 'test-client-id',
        clientSecret: 'very-secret',
        redirectUri: 'http://127.0.0.1:8080/'
    }
]

const users = [
    {
        email: 'testuser@asd.com',
        password: 'p4ssw0rd'
    }
]

for (let client of clients) {
    ClientService.create(client.appName, client.clientId, client.clientSecret, client.redirectUri);
}
for (let user of users) {
    UserService.create(user.email, user.password);
}
