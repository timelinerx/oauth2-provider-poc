import MockService from "./mock.service";
import Client from "../models/client.model";
import {clients} from "./mock-db";

class MockClientService extends MockService<Client> {
    create(...args: ConstructorParameters<typeof Client>): Client {
        const client = new Client(...args);
        clients.push(client);
        return client;
    }

    delete(id: string): void {
        clients.remove(id);
    }

    getBy(key: keyof Client, value: Client[keyof Client]): Client {
        return clients.findBy(key, value);
    }

    update(id: string, updateValues: Partial<Client>): Client {
        let client = clients.findBy('id', id);
        client = {...client, ...updateValues};
        return client;
    }
}

export const ClientService = new MockClientService();
