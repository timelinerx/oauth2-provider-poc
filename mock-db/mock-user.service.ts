import MockService from "./mock.service";
import User from "../models/user.model";
import {users} from "./mock-db";

class MockUserService extends MockService<User> {
    create(...args: ConstructorParameters<typeof User>): User {
        if (users.findBy('email', args[0])) throw new Error('User already exists!');
        const user = new User(...args);
        users.push(user);
        return user;
    }

    delete(id: string) {
        users.remove(id);
    }

    getBy(key: keyof User, value: User[keyof User]): User {
        return users.findBy(key, value);
    }

    update(id: string, updateValues: Partial<User>): User {
        let user = users.findBy('id', id);
        user = {...user, ...updateValues};
        return user;
    }
}

export const UserService = new MockUserService();
