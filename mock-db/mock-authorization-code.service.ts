import MockService from "./mock.service";
import AuthorizationCode from "../models/authorization-code.model";
import {authorizationCodes} from "./mock-db";

class MockAuthorizationCodeService extends MockService<AuthorizationCode> {
    create(...args: ConstructorParameters<typeof AuthorizationCode>): AuthorizationCode {
        const code = new AuthorizationCode(...args);
        authorizationCodes.push(code);
        return code;
    }

    delete(id: string): void {
        authorizationCodes.remove(id);
    }

    getBy(key: keyof AuthorizationCode, value: AuthorizationCode[keyof AuthorizationCode]): AuthorizationCode {
        return authorizationCodes.findBy(key, value);
    }

    update(id: string, updateValues: Partial<AuthorizationCode>): AuthorizationCode {
        let code = authorizationCodes.findBy('id', id);
        code = {...code, ...updateValues};
        return code;
    }

}

export const AuthorizationCodeService = new MockAuthorizationCodeService();
