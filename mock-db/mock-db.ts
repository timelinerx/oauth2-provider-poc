import AuthorizationCode from "../models/authorization-code.model";
import User from "../models/user.model";
import AccessToken from "../models/access-token.model";
import Client from "../models/client.model";


export class MockDb<T> extends Array<any> {
    constructor() {
        super();
        Object.setPrototypeOf(this, MockDb.prototype)
    }
    findBy(key: keyof T, value: T[keyof T]): T {
        return this.find(obj => obj[key] === value)
    }
    remove(id: string) {
        this.splice(this.findIndex(obj => obj.id === id), 1)
    }
}

export const users = new MockDb<User>();
export const authorizationCodes = new MockDb<AuthorizationCode>();
export const accessTokens = new MockDb<AccessToken>()
export const clients = new MockDb<Client>();
