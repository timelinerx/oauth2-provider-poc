import Model from "./model";

export default class Client extends Model {
    constructor(
        public appName: string,
        public clientId: string,
        public clientSecret: string,
        public redirectURI: string | string[]) {
        super();
    }
}
