import Model from "./model";

export default class User extends Model {
    constructor(
        public email: string,
        public password: string
    ) {
        super();
    }
}
