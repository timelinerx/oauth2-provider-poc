import {v4 as uuidV4} from "uuid";

export default abstract class Model {
    public readonly id: string;
    protected constructor() {
        this.id = uuidV4();
    }
}
