import Model from "./model";

export default class AuthorizationCode extends Model {
    constructor(
        public code: string,
        public clientId: string,
        public redirectURI: string,
        public userId: string,
        public scope: string | string[],
        public codeChallenge?: string,
        public codeChallengeMethod?: string
    ) {
        super();
    }
}
