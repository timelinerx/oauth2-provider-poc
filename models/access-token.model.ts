import Model from "./model";

export default class AccessToken extends Model {
    constructor(
        public token: string,
        public userId: string,
        public clientId: string,
        public scope: string | string[]) {
        super();
    }
}
