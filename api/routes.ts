import Router from 'koa-router'
import {authorizationServer} from "../oauth2orize/oauth2orize";
import {
    authorizeClient,
    ensureLogin, getDummyProjects, getUserInfo,
    loginUser,
    registerUser,
    renderIndex,
    renderLogin,
    renderPermissionDialog,
    renderRegister,
    renderUser,
    session
} from "./controller";
import passport from "koa-passport";

const router = new Router();

router.use(session);
router.use('/api', passport.authenticate('bearer', { session: false }))

// GET /
router.get('/', renderIndex);

// GET /register
router.get('/register', renderRegister);

// POST /register
router.post('/register', registerUser);

// GET /login
router.get('/login', renderLogin);

// POST /login
router.post('/login', loginUser);

// GET /user
router.get('/user',
    ensureLogin,
    renderUser
);

// GET /authorize
router.get('/authorize',
    ensureLogin,
    authorizeClient(),
    renderPermissionDialog
);

// POST /authorize/decision
router.post('/authorize/decision',
ensureLogin,
...authorizationServer.decision()
);

// POST /token
router.post('/token',
    passport.authenticate(['basic', 'oauth2-client-password', 'oauth2-client-public'], {session: false}),
    authorizationServer.token(),
    authorizationServer.errorHandler()
)

// GET /api/userinfo
router.get('/api/userinfo', getUserInfo)

// GET /api/projects
router.get('/api/projects', getDummyProjects)

export default router;
