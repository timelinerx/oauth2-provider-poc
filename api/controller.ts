import {UserService} from "../mock-db/mock-user.service";
import {authorizationServer} from "../oauth2orize/oauth2orize";
import {ClientService} from "../mock-db/mock-client.service";

enum VIEWS {
    INDEX = 'index',
    REGISTER = 'register',
    REGISTER_SUCCESS = 'register-success',
    REGISTER_FAIL = 'register-fail',
    LOGIN = 'login',
    LOGIN_DIALOG = 'login-dialog',
    USER = 'user',
    PERMISSION_DIALOG = 'permission-dialog',
}

export async function renderIndex(ctx) {
    await ctx.render(VIEWS.INDEX);
}

export async function renderRegister(ctx) {
    await ctx.render(VIEWS.REGISTER)
}

export async function renderLogin(ctx) {
    await ctx.render(VIEWS.LOGIN)
}

export async function renderUser(ctx) {
    await ctx.render(VIEWS.USER, {email: ctx.session.user.email})
}

export async function renderPermissionDialog(ctx) {
    const {transactionID, client} = ctx.state.oauth2;
    ctx.state.user = ctx.session.user;
    await ctx.render(VIEWS.PERMISSION_DIALOG, {
        appName: client.appName,
        transactionID,
        clientId: client.clientId
    })
}

export async function registerUser(ctx) {
    const {email, password} = ctx.request.body;
    try {
        UserService.create(email, password);
        await ctx.render(VIEWS.REGISTER_SUCCESS)
    } catch (err) {
        await ctx.render(VIEWS.REGISTER_FAIL, { error: err.message })
    }
}

export async function loginUser(ctx) {
    const {email, password} = ctx.request.body;
    const user = UserService.getBy('email', email);
    if (!user || user.password !== password) {
        await ctx.render(VIEWS.LOGIN, {error: 'Invalid credentials'})
        return;
    }
    ctx.session.userId = user.id;
    if (ctx.session.returnTo) {
        ctx.redirect(ctx.session.returnTo);
        delete ctx.session.returnTo;
    } else {
        ctx.redirect('/user');
    }
}

export function authorizeClient() {
    return authorizationServer.authorize(async (clientId, redirectURI) => {
        const client = ClientService.getBy('clientId', clientId);
        if (!client) throw new Error('Invalid client ID!');
        if (client.redirectURI !== redirectURI) throw new Error('Redirect URI mismatch!');
        return [client, client.redirectURI];
    })
}

export async function getDummyProjects(ctx) {
    const projects: any[] = [];
    for (let i = 0; i < 12; i++) {
        projects.push({
            name: `Project - ${i}`,
            id: i
        })
    }
    ctx.body = projects;
}

export async function ensureLogin(ctx, next) {
    if (!ctx.session.user) {
        ctx.session.returnTo = ctx.request.url;
        await ctx.render(VIEWS.LOGIN_DIALOG);
        return;
    } else {
        // oauth2orize-koa requires the user object on the ctx.state object
        ctx.state.user = ctx.session.user;
        await next();
    }
}

export async function session(ctx, next) {
    if (ctx.session || ctx.session.userId) {
        const user = UserService.getBy('id', ctx.session.userId);
        if (user) {
            ctx.session.user = user;
        }
    }
    await next();
}

export async function getUserInfo(ctx) {
    ctx.body = {email: ctx.state.user.email};
 }
